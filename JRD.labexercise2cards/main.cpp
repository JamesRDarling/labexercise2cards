// James Darling
// Lab Exercise 2 Cards

#include <iostream>
#include <vector>
#include <string>
#include <conio.h>

using namespace std;


enum Rank { TWO = 1, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE = 14 };
enum Suit { SPADES = 1, HEARTS, DIAMONDS, CLUBS };

struct Card
{
	
	Rank rank;
	Suit suit;
	int num_suits = 5;
	int num_ranks = 15;
};

struct Deck
{
	vector<Card> cards;
	string card_back;
	int max_card = 52;
};

void initialize(Deck&);
void print_deck(const Deck&);
void print_card(const Card&);

int main()
{
	Deck my_deck;
	initialize(my_deck);
	print_deck(my_deck);
	cout << "Press any key to exit";
	cin.get();
	return 0;
}

void initialize(Deck& deck)
{
	Card card;
	for (int suit = 1; suit < card.num_suits; suit++)
	{
		for (int rank = 1; rank < card.num_ranks; rank++)
		{
				card.suit = Suit(suit);
				card.rank = Rank(rank);
				deck.cards.push_back(card);
		}
	}
	
}

void print_deck(const Deck& deck)
{
	for (Card c: deck.cards)
	{
		print_card(c);
	}
}

void print_card(const Card& card)
{
	cout << "Rank = " << card.rank << "   " << "Suit = " << card.suit << '\n';
}